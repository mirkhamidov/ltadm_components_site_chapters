<?
$items=array(
  "name" => array(
         "desc" => "Название раздела",
         "type" => "text",
         "readonly" => true,
         "maxlength" => "255",
         "size" => "30",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),
  "url" => array(
         "desc" => "Идентификатор раздела",
         "type" => "text",
         "readonly" => true,
         "maxlength" => "50",
         "size" => "10",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и '_','-'!",
           ),
         ),
       ),
  "is_default" => array(
         "desc" => "Использовать по умолчанию?",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "no",
         "select_on_edit" => true,
       ),

  "content" => array(
         "desc" => "Текст на главную страницу",
         "type" => "editor",
         "width" => "100%",
         "height" => "500",
         "select_on_edit" => true,
       ),


);
?>