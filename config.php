<?
$config=array(
  "name" => "Разделы сайта",
  "actions" => array("select"),
  "no_delete" => true,
  "windows" => array(
                      "create" => array("width" => 700,"height" => 300),
                      "edit" => array("width" => 600,"height" => 300),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_site_chapters",
  "select" => array(
     "default_orders" => array(
                           array("name" => "ASC"),
                         ),
     "default" => array(
        "id_lt_site_chapters" => array(
               "desc" => "Раздел сайта",
               "type" => "select_from_table",
               "table" => "lt_site_chapters",
               "key_field" => "id_lt_site_chapters",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "",
               "order" => array ("name" => "ASC"),
               "use_empty" => true,
             ),

     ),
  ),
);

$actions=array(
  "create" => array(

    "before_code" => "",

  ),
);



?>